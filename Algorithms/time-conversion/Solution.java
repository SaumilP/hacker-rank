import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        String time = in.next();
        in.close();

        System.out.println(convertTime(time));
    }

    private static String convertTime(String inputTime) throws ParseException {
        String time = "";
        DateFormat readFormat = new SimpleDateFormat( "hh:mm:ssaa");
        DateFormat writeFormat = new SimpleDateFormat( "HH:mm:ss");
        Date date = readFormat.parse(inputTime);

        if (date != null) {
            time = writeFormat.format(date);
        }
        return time;
    }
}
