import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Total number of elements for problem
        int totalNumbers = in.nextInt();

        // Numbers
        int arr[] = new int[totalNumbers];
        for (int arr_i = 0; arr_i < totalNumbers; arr_i++) {
            arr[arr_i] = in.nextInt();
        }
        in.close();

        //print average
        int noOfPositives = getNumberOfPositives(arr);
        System.out.println(String.format("%.6f", noOfPositives*1.0/totalNumbers));

        int noOfNegatives = getNumberOfNegatives(arr);
        System.out.println(String.format("%.6f", noOfNegatives*1.0/totalNumbers));

        int noOfZeros = getNumberOfZero(arr);
        System.out.println(String.format("%.6f", noOfZeros*1.0/totalNumbers));
    }

    private static int getNumberOfZero(int[] array) {
        int res = 0;
        for (int number : array) {
            if(number == 0){
                res++;
            }
        }
        return res;
    }

    private static int getNumberOfNegatives(int[] array) {
        int res = 0;
        for (int number : array) {
            if(number < 0){
                res++;
            }
        }
        return res;
    }

    private static int getNumberOfPositives(int[] array) {
        int res = 0;
        for (int number : array) {
            if(number > 0){
                res++;
            }
        }
        return res;
    }
}
