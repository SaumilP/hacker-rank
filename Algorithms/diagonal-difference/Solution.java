import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      int n = in.nextInt();
      int a[][] = new int[n][n];
      for(int a_i=0; a_i < n; a_i++){
          for(int a_j=0; a_j < n; a_j++){
              a[a_i][a_j] = in.nextInt();
          }
      }
      in.close();

      int diagonalDiff = findDiagonalDifference(a);
      System.out.println(diagonalDiff);
  }

  private static int findDiagonalDifference(int arr[][]){
      if (arr == null || arr.length == 0){
          return 0;
      }

      int[] leftDiagonalValues = getDiagonalValues(arr, true);
      int[] rightDiagonalValues = getDiagonalValues(arr, false);

      int fromLeft = getNumberSum(leftDiagonalValues), fromRight = getNumberSum(rightDiagonalValues);
      return Math.abs(fromLeft - fromRight);
  }

  private static int[] getDiagonalValues(int arr[][], boolean fromLeft){
      int size = arr[0].length;
      int res[] = new int[size];
      int res_index=0, rev_index = size-1;
      for(int row=0; row < arr[0].length; row++){
          for(int col=0; col < arr[row].length; col++){
              if(fromLeft && row == res_index && col == res_index){
                  res[res_index++] = arr[row][col];
                  break;
              }
              if (!fromLeft && col == rev_index && row == res_index){
                  res[res_index++] = arr[row][col];
                  rev_index--;
                  break;
              }
          }
      }
      return res;
  }

  private static int getNumberSum(int[] num_arr){
      int res = 0;
      for (int aNum_arr : num_arr) {
          res += aNum_arr;
      }
      return res;
  }
}
