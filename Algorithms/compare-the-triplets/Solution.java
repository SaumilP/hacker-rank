import java.util.Scanner;

public class CompareTriplets {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a0 = in.nextInt();
        int a1 = in.nextInt();
        int a2 = in.nextInt();
        int b0 = in.nextInt();
        int b1 = in.nextInt();
        int b2 = in.nextInt();
        in.close();

        int alice = 0, bob = 0;
        if( isWithinRange(1, a0, 100) && isWithinRange(1, b0, 100) ){
            if(givePointToAlice(a0, b0)){
                alice += 1;
            } else if(givePointToBob(a0, b0)){
                bob += 1;
            }
        }
        if ( isWithinRange(1, a1, 100) && isWithinRange(1, b1, 100) ){
            if(givePointToAlice(a1, b1)){
                alice += 1;
            } else if(givePointToBob(a1, b1)){
                bob += 1;
            }
        }
        if ( isWithinRange(1, a2, 100) && isWithinRange(1, b2, 100) ){
            if(givePointToAlice(a2, b2)){
                alice += 1;
            } else if(givePointToBob(a2, b2)){
                bob += 1;
            }
        }
        System.out.println(String.format("%d %d", alice, bob));
    }

    private static boolean isWithinRange(int startRange, int value, int endRange){
        return startRange <= value && value <= endRange;
    }

    private static boolean givePointToAlice(int num1, int num2){
        return num1 > num2 ;
    }

    private static boolean givePointToBob(int num1, int num2){
        return num1 < num2;
    }
}
