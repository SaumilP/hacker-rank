import java.util.Arrays;
import java.util.Scanner;

public class Staircase {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int noOfLevels = in.nextInt();
        in.close();

        for (int i = 1; i <= noOfLevels; i++) {
            final String spaces = repeat("", ' ', noOfLevels);
            final String characters = repeat("", '#', noOfLevels);
            final String formattedStr = spaces+characters;
            System.out.println(String.format(formattedStr).substring(i, i+noOfLevels));
        }
    }

    private static String repeat(String result, char character, int size){
        char[] repeat = new char[size];
        Arrays.fill(repeat, character);
        result += new String(repeat);
        return result;
    }
}
