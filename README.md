# HackerRank Practice work #

This repository contains different problems attempted on [HackerRank](http://www.hackerrank.com).

###Content###

Mostly all the problems are based on associated competition or program.

* bash
* 30 Days of Code
* Algorithms
* ....

###How to debug###

IDEs like IntelliJ or Eclipse can be used to execute and debug Java based solutions.

For bash scripts, below method can be used for debugging.
```
/bin/bash -x solution.sh
```
