#!/bin/bash
read expression
val=`echo "scale=4; $expression" | bc`
printf "%.3f\n" $val
