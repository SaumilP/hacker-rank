#!/bin/bash
read tot_nums
result=0
i=0
while [ $((i)) -lt $((tot_nums)) ]
do
  read num
  result=$((result+num))
  i=$((i+1))
done

res=`echo "scale=4; $result/$tot_nums" | bc`
printf "%.3f\n" $res
