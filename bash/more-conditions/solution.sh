#!/bin/bash
read num1
read num2
read num3
if [ "1" -le "$((num1))" ] && [ "1" -le "$((num2))" ] && [ "1" -le "$((num3))" ]
then
  if [ "$((num1))" -lt "$((num2))" ] && [ "$((num2))" -lt "$((num3))" ]
  then
    echo "SCALENE"
  elif [ "$((num1))" -eq "$((num2))" ] && [ "$((num2))" -eq "$((num3))" ]
  then
    echo "EQUILATERAL"
  else
    echo "ISOSCELES"
  fi
fi
