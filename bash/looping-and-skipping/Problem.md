for loops in Bash can be used in several ways:
- iterating between two integers, `a` and `b`
- iterating between two integers, `a` and `b`, and incrementing by `c` each time
- iterating through the elements of an array, etc.

Your task is to use for loops to display only odd natural numbers from  to .

**Input**

There is no input.

**Output**
```
1
3
5
.
.
.
.
.
99  
```
**Recommended Resources**

A quick but useful tutorial for bash newcomers is [here](https://www.hackerrank.com/external_redirect?to=http://www.panix.com/~elflord/unix/bash-tute.html).

Handling input is documented and explained quite well on [this page](https://www.hackerrank.com/external_redirect?to=http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_08_02.html).

Different ways in which for loops may be used are explained with examples [here](https://www.hackerrank.com/external_redirect?to=http://www.cyberciti.biz/faq/bash-for-loop/).
