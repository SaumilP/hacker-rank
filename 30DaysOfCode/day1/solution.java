import java.util.Scanner;

class Solution {
  ublic static void main(String[] args) {
        int i = 4;
        double d = 4.0;
        String s = "HackerRank ";

        Scanner scan = new Scanner(System.in);

        /* Declare second integer, double, and String variables. */
        int iValue = Integer.parseInt(scan.nextLine());
        double dValue = Double.parseDouble(scan.nextLine());
        String sValue = scan.nextLine();

        /* Read and save an integer, double, and String to your variables.*/
        int iResult = i + iValue;
        double dResult = d + dValue;
        String sResult = s + sValue;

        /* Print the sum of both integer variables on a new line. */
        System.out.println(iResult);

        /* Print the sum of the double variables on a new line. */
        System.out.println(dResult);

        /* Concatenate and print the String variables on a new line;
            the 's' variable above should be printed first. */
        System.out.println(sResult);

        scan.close();
    }
}
